//
//  ViewController.swift
//  CursoApp
//
//  Created by Oscar on 10/10/22.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var dado1: UIImageView!
    @IBOutlet weak var dado2: UIImageView!
    @IBOutlet weak var dado3: UIImageView!
    @IBOutlet weak var dado4: UIImageView!
    @IBOutlet weak var dado5: UIImageView!
    @IBOutlet weak var tirarDadosBtn: UIButton!
    
    @IBAction func tirarDados(_ sender: UIButton) {
        let dados = [dado1, dado2, dado3, dado4, dado5]
        
        for dado in dados {
            let dadosString = [1:"DiceOne", 2:"DiceTwo", 3:"DiceThree", 4:"DiceFour", 5:"DiceFive"]
            let numeroAleatorio = Int.random(in: 1...5)
            dado!.image = UIImage(imageLiteralResourceName: dadosString[numeroAleatorio]!)
        }
    }
}

